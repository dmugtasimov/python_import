#!/bin/bash

echo 'import abcd: case 1'
pushd import_abcd_1
./run.sh
popd
echo ''

echo 'import abcd: case 2'
pushd import_abcd_2
./run.sh
popd
echo ''

echo 'import abcd: case 4'
pushd import_abcd_4
./run.sh
popd
echo ''

echo 'import abcd: case 5'
pushd import_abcd_5
./run.sh
popd
echo ''

echo 'import abcd.defg: case 1'
pushd import_abcd_defg_1
./run.sh
popd
echo ''

echo 'import abcd.defg: case 2'
pushd import_abcd_defg_2
./run.sh
popd
echo ''

echo 'from abcd.defg import ghi: case 1.1'
pushd from_abcd_defg_import_ghi_1_1
./run.sh
popd
echo ''

echo 'from abcd.defg import ghi: case 1.2'
pushd from_abcd_defg_import_ghi_1_2
./run.sh
popd
echo ''

echo 'from abcd.defg import ghi: case 2'
pushd from_abcd_defg_import_ghi_2
./run.sh
popd
echo ''

echo 'from abcd.defg import ghi: case 3'
pushd from_abcd_defg_import_ghi_3
./run.sh
popd
echo ''
